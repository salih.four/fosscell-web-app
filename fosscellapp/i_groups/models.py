from django.db import models
from django_resized import ResizedImageField
from users.models import FOSSCellPermission, FOSSCellMember
from django.templatetags.static import static

IG_STATUS_LIST = [
  ('suggested', 'Suggested'),
  ('approved', 'Suggestion approved'),
]

class InterestGroup(models.Model):
  ADMIN_PERM = 'admin'

  topic = models.CharField(max_length=50)
  badge = ResizedImageField(size=[150, 150], crop=['middle', 'center'], upload_to='static/i_groups/badges/', null=True)
  description = models.TextField(max_length=1200)
  members = models.ManyToManyField(to='users.FOSSCellMember', through='InterestGroupMembership', through_fields=('interest_group', 'member'))
  status = models.CharField(max_length=20, choices=IG_STATUS_LIST, default='suggested', null=False)

  def is_approved(self):
    if self.status == 'approved':
      return True
    else:
      return False
  
  def mark_as_approved(self):
    group = InterestGroup.objects.get(pk=self.pk)
    group.status = 'approved'
    group.save()

  def delete_if_unapproved(self):
    group = InterestGroup.objects.get(pk=self.pk)
    if not group.is_approved():
      group.delete()
      
  @staticmethod
  def get_list():
    query_set = InterestGroup.objects.filter(status='approved')
    return [i for i in query_set]

  def get_suggested_list(self):
    query_set = InterestGroup.objects.filter(status='suggested')
    return [i for i in query_set]

  def get_badge_url(self):
    if self.badge and hasattr(self.badge, 'url'):
      return '/'+self.badge.url
    else:
      return static('i_groups/badges/none.png')

  def add_member(self, member, is_approved=False):
    self.members.add(member, through_defaults={'is_approved': is_approved})

  def remove_member(self, member):
    admin_perm = FOSSCellPermission.get_ig_permission(self, InterestGroup.ADMIN_PERM)
    if member.verify_perm(admin_perm):
      admin_count = FOSSCellPermission.get_ig_permission_count(self, InterestGroup.ADMIN_PERM)
      if admin_count == 1:
        raise Exception('removing-the-only-admin')
        return
    self.members.remove(member)

  def save(self, *args, **kwargs):
    # Deleting old badge before update
    if self.pk:
      old_data = InterestGroup.objects.get(pk=self.pk)
      if self.badge != old_data.badge:
        old_image_path = old_data.get_badge_url()
        if old_image_path:
          try:
            os.remove(old_image_path)
          except OSError as e:
            if e.errno != errno.ENOENT:
              raise
      super(InterestGroup, self).save(*args, **kwargs)
    else:
      super(InterestGroup, self).save(*args, **kwargs)
      
      # Create and admin permission object for new group
      perm = FOSSCellPermission.create_ig_permission(self, InterestGroup.ADMIN_PERM)
      perm.save()
    return

  def get_member_status(self, user, *args, **kwargs):
    try:
      membership = InterestGroupMembership.objects.get(interest_group=self, member=user)
      return 'approved' if membership.is_approved else 'requested'
    except InterestGroupMembership.DoesNotExist:
      return 'stranger'

  def get_members(self):
    memberships = InterestGroupMembership.objects.filter(interest_group=self, is_approved=True)
    return [m.member for m in memberships]

  def get_requested_members(self):
    memberships = InterestGroupMembership.objects.filter(interest_group=self, is_approved=False)
    return [m.member for m in memberships]

  def approve_request(self, member):
    try:
      membership = InterestGroupMembership.objects.get(interest_group=self, member=member, is_approved=False)
      membership.is_approved = True
      membership.save()
    except InterestGroupMembership.DoesNotExist:
      pass

  @staticmethod
  def get_igs_of_user(user, is_approved=True):
    memberships = InterestGroupMembership.objects.filter(member=user, is_approved=is_approved)
    igs = [m.interest_group for m in memberships]
    return igs

  @staticmethod
  def get_suggestions_for_user(user):
    approved_igs = InterestGroup.get_list()
    found = []
    for ig in approved_igs:
        if ig.get_member_status(user) == 'stranger':
            found += [ig]
    return found  

class InterestGroupMembership(models.Model):
  interest_group = models.ForeignKey(InterestGroup, on_delete=models.CASCADE)
  member = models.ForeignKey(FOSSCellMember, on_delete=models.CASCADE)
  is_approved = models.BooleanField(default=False)