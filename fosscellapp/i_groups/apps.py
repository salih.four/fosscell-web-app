from django.apps import AppConfig


class IGroupsConfig(AppConfig):
    name = 'i_groups'
