from django import forms

class EditIGInfoForm(forms.Form):
  topic = forms.CharField(label='Topic', max_length=50, required=True)
  description = forms.CharField(widget=forms.Textarea, label='Description', max_length=1200, required=False)

class EditIGBadgeForm(forms.Form):
  badge = forms.ImageField(label='Group badge image (A size of 150x150 px expected)')