# Generated by Django 3.0.4 on 2020-04-05 15:54

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('i_groups', '0001_create_interest_group'),
    ]

    operations = [
        migrations.CreateModel(
            name='InterestGroupMembership',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('is_approved', models.BooleanField(default=False)),
                ('interest_group', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='i_groups.InterestGroup')),
                ('member', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='interestgroup',
            name='members',
            field=models.ManyToManyField(through='i_groups.InterestGroupMembership', to=settings.AUTH_USER_MODEL),
        ),
    ]
