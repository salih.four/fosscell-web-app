from django.http import HttpResponse, JsonResponse
from django.views import View
from django.shortcuts import render
#importing loading from django template 
from django.template import loader
from users.models import FOSSCellMember
from django.contrib.auth.decorators import login_required
from django.db.models.functions import Concat
from i_groups.models import InterestGroup
from django.db.models import Value as V
from dash.templatetags.interestgroup import member_url, ig_url
import re
import json

#our view which is a function named index

class SignUpView(View):
    def get(self, request, *args, **kwargs):
        template = 'signup.html'
        return render(request, template, {})    

    def post(self, request, *args, **kwargs):
        data = request.POST
        email = data['email']
        password = data['pass']
        nitc_relation = data['nitc-relation']
        fullname = data['fullname']
        nickname = data['dname']
        contact = data['phone']

        new_member = FOSSCellMember(
            email = email,
            password = password,
            name = fullname,
            mobile = contact,
            dname = nickname,
            nitc_relation = nitc_relation
        )
        new_member.save()

        context = {
            "info": "Account created"
        }
        template = 'info.html'
        return render(request, template, context)

def home(request):
    template = loader.get_template('nova/index.html')
    return HttpResponse(template.render())

class FOSSCellSearchApi(View):
  def get(self, request, *args, **kwargs):
    @login_required
    def _get(request, self, *args, **kwargs):
      query = request.GET.get('q')
      if not query:
        return HttpResponse('You did not give any query')
      what = []
      result = []
      if "member:" in query:
        what.append('member')
      if "group:" in query:
        what.append('group')
      query = query.replace("group:", "")
      query = query.replace("member:", "")
      query = " ".join(query.split())
      print(query)
      members_query_set = None
      groups_query_set = None
      if 'member' in what or len(what) == 0:
        members_query_set = FOSSCellMember.objects.annotate(
            keywords=Concat(
              'username', V(' '),
              'first_name', V(' '),
              'last_name')
          ).filter(keywords__icontains=query)[:8]
      if 'group' in what or len(what) == 0:
        groups_query_set = InterestGroup.objects.filter(topic__icontains=query)[:8]
      result_set = []
      def make_result_item(**kwargs):
          return {key: kwargs[key] for key in kwargs}

      if members_query_set:
        result_set += [
          make_result_item(
            kind='member',
            url=member_url(m.username),
            username=m.username,
            full_name=m.get_full_name(),
            avatar=m.get_avatar_url(),
            nitc_relation=m.nitc_relation
          ) for m in members_query_set
        ]
      if groups_query_set:
        result_set += [
          make_result_item(
            kind='interest group',
            url=ig_url(g.pk),
            topic=g.topic,
            badge=g.get_badge_url(),
            pkey=g.pk
          ) for g in groups_query_set
        ]
      return JsonResponse({'results': result_set})
    return _get(request, self, *args, **kwargs)