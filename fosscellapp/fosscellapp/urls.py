"""fosscellapp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from .views import SignUpView, FOSSCellSearchApi
from django.views.generic.base import TemplateView
from django.conf.urls.static import static
from django.conf import settings
import os

urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include('allauth.urls')),
    path('dash/', include('dash.urls')),
    path('signup/', SignUpView.as_view()),
    path('search-api/', FOSSCellSearchApi.as_view(), name='search-api'),
    path('', TemplateView.as_view(template_name='nova/fosscell-home.html'), name='fosscell-home'),
]

if os.environ.get('DJANGO_ENV') == 'development':
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)