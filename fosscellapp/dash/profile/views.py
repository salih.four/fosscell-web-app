from django.shortcuts import render, redirect
from django.views import View
from django.http import HttpResponse
from users.forms import EditPersonalDetailsForm, EditNITCRelationForm, EditAvatarForm
from users.models import FOSSCellMember

# Create your views here.
class EditProfileView(View):
  
  def get(self, request, *args, **kwargs):
    requested_form = request.GET.get('form', 'personal-details')
    template = 'dash/pages/django_form.html'
    form = None
    title = None
    enctype = None
    if requested_form == 'personal-details':
      form = EditPersonalDetailsForm()
      title = 'Edit personal details'
    elif requested_form == 'nitc-relation':
      form = EditNITCRelationForm()
      title = 'Edit relationship with NITC'
    elif requested_form == 'avatar':
      form = EditAvatarForm()
      title = 'Edit Avatar'
      enctype = 'multipart/form-data'
    context = { 
      'form': form,
      'action': '',
      'method': 'post',
      'enctype': enctype,
      'title': title,
      'identifier': requested_form
      }
    return render(request, template, context)
  
  def post(self, request, *args, **kwargs):
    identifier = request.POST.get('identifier', None)
    fosscell_member = FOSSCellMember.objects.get(username=request.user.username)
    enctype = None
    title = None
    if identifier == 'personal-details':
      title = 'Edit personal details'
      form = EditPersonalDetailsForm(request.POST)
      if form.is_valid():
        fosscell_member.first_name = form.cleaned_data['first_name']
        fosscell_member.last_name = form.cleaned_data['last_name']
        fosscell_member.mobile = form.cleaned_data['mobile']
        fosscell_member.save()
        return redirect('../profile/')
    elif identifier == 'nitc-relation':
      title = 'Edit relationship with NITC'
      form = EditNITCRelationForm(request.POST)
      if form.is_valid():
        fosscell_member.nitc_relation = form.cleaned_data['nitc_relation']
        fosscell_member.roll_no = form.cleaned_data['roll_no']
        fosscell_member.save()
        return redirect('../profile/')
    elif identifier == 'avatar':
      enctype = 'multipart/form-data'
      title = 'Edit Avatar'
      form = EditAvatarForm(request.POST, request.FILES)
      if form.is_valid():
        fosscell_member.avatar = form.cleaned_data['avatar']
        fosscell_member.save()
        return redirect('../profile/')
    context = { 
      'form': form,
      'action': '',
      'method': 'post',
      'enctype': enctype,
      'title': title,
      'identifier': identifier
      }
    template = 'dash/pages/django_form.html'
    return render(request, template, context)