from django.views import View
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.urls import reverse
from users.models import FOSSCellMember, FOSSCellPermission
from users.designations import IGsModerator
from django.core.exceptions import PermissionDenied

class SetupView(View):
  def get(self, request, *args, **kwargs):
    if request.user.is_superuser == True:
      template = 'dash/pages/setup.html'
      context = {
        'fosscell_chief': FOSSCellPermission.get_fosscell_chief()
      }
      return render(request, template, context)
    else:
      return HttpResponse('Not allowed')

  def post(self, request, *args, **kwargs):
    if request.user.is_superuser == True:
      if request.POST.get('submit') == 'assign-fosscell-chief':
        username = request.POST.get('username')
        if username:
          try:
            user = FOSSCellMember.objects.get(username=username)
            user.assign_as_fosscell_chief()
            return redirect(reverse('setup'))
          except FOSSCellMember.DoesNotExist:
            return HttpResponse('No such member ' + username)
    else:
      return HttpResponse('Not allowed')

class MoreLinksView(View):
  def get(self, request, *args, **kwargs):
    template = 'dash/pages/more-links.html'
    class Link():
      def __init__(self, title, url):
        self.title = title
        self.url = url
    more_links = []
    if request.user.is_superuser:
      more_links.append(Link("Superuser's Setup", reverse('setup')))
    if request.user.is_fosscell_chief():
      more_links.append(Link("Chief's Office", reverse('chief-office')))
    try:
      IGsModerator(request.user)
      more_links.append(Link("IGM's Office", reverse('igm-office')))
    except PermissionDenied:
      pass

    context = {
      'more_links': more_links if len(more_links) > 0 else None
    }
    print(more_links)
    return render(request, template, context)