from django.urls import path, include
from django.views.generic.base import TemplateView
from django.contrib.auth.decorators import login_required
from users.decorators import fosscell_chief_permission_required
from .profile.views import EditProfileView
from .views import SetupView, MoreLinksView

urlpatterns = [
    path('', login_required(TemplateView.as_view(template_name='dash/pages/dash.html')), name='dashboard'),
    path('profile/', login_required(TemplateView.as_view(template_name='dash/pages/profile.html')), name='profile'),
    path('edit-profile/', login_required(EditProfileView.as_view())),
    path('interest-groups/', include('dash.i_groups.urls')),
    path('more-links/', login_required(MoreLinksView.as_view()), name='more-links'),
    path('setup/', login_required(SetupView.as_view()), name='setup'),
    path('office/', include('dash.office.urls')),
    path('forms/', login_required(TemplateView.as_view(template_name='dash/pages/forms.html'))),
    path('ui/', login_required(TemplateView.as_view(template_name='dash/pages/ui.html'))),
    path('chat/', login_required(TemplateView.as_view(template_name='dash/pages/chat.html'))),
]