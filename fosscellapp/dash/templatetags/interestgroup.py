from django import template
from django.urls import reverse
register = template.Library()

IG_BASE_URL = '/dash/interest-groups/'

@register.simple_tag
def ig_url(pkey):
  return '{}{}/'.format(IG_BASE_URL, str(pkey))

@register.simple_tag
def ig_edit_url(pkey, form):
  if pkey == None:
    if form == 'suggestion':
      return '{}suggest/?form={}'.format(IG_BASE_URL, form)
  else:
    return '{}edit/?form={}'.format(ig_url(pkey), form)

@register.simple_tag
def ig_nav_url(pkey, page):
  return '{}{}/'.format(ig_url(pkey), page)

@register.simple_tag
def ig_member_action_url(pkey, action):
  return '{}member-action/{}/'.format(ig_url(pkey), action)

@register.simple_tag
def member_url(username):
  return '{}members/{}/'.format(reverse('dashboard'), username)
  