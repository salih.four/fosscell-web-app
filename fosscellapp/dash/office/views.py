from django.views import View
from users.decorators import fosscell_chief_permission_required
from users.designations import FOSSCellChief, IGsModerator
from users.models import FOSSCellPermission, FOSSCellMember
from django.shortcuts import render, redirect
from django.urls import reverse
from i_groups.models import InterestGroup

class ChiefOfficeView(View):
  
  def get(self, request, *args, **kwargs):
    FOSSCellChief(request.user) # Authorisation
    template = 'dash/pages/chief-office.html'
    igm_list = FOSSCellPermission.get_igms_list()
    context = {
      'igm_list': igm_list
    }
    return render(request, template, context)

  def post(self, request, *args, **kwargs):
    
    chief = FOSSCellChief(request.user) # Authorisation
    
    def remove_ig_moderator(request, chief, *args, **kwargs):
      igm = FOSSCellMember.objects.get(username=request.POST.get('username'))
      chief.undesignate_user_as_igs_moderator(igm)
      return redirect(reverse('chief-office'))

    def assign_ig_moderator(request, chief, *args, **kwargs):
      assignee = FOSSCellMember.objects.get(username=request.POST.get('username'))
      chief.designate_user_as_igs_moderator(assignee)
      return redirect(reverse('chief-office'))

    action = request.POST.get('action')
    
    # Action: Assign a user as IGM
    if action == 'assign-igm':
      return assign_ig_moderator(request, chief, *args, **kwargs)

    elif action == 'remove-igm':
      return remove_ig_moderator(request, chief, *args, **kwargs)

class IGsModeratorOfficeView(View):

  def get(self, request, *args, **kwargs):
    moderator = IGsModerator(request.user) # Authorisation
    template = 'dash/pages/igm-office.html'
    unapproved_igs_list = moderator.get_unapproved_igs_list()
    context = {
      'unapproved_igs_list': unapproved_igs_list
    }
    return render(request, template, context)

  def post(self, request, *args, **kwargs):
    moderator = IGsModerator(request.user) # Authorisation
    
    def approve_interest_group(request, moderator, *args, **kwargs):
      group = InterestGroup.objects.get(pk=request.POST.get('pkey'))
      moderator.approve_interest_group(group)
      return redirect(reverse('igm-office'))

    def delete_suggested_ig(request, moderator, *args, **kwargs):
      group = InterestGroup.objects.get(pk=request.POST.get('pkey'))
      moderator.delete_unapproved_interest_group(group)
      return redirect(reverse('igm-office'))

    def reverse_approval(request, moderator, *args, **kwargs):
      group = InterestGroup.objects.get(pk=request.POST.get('pkey'))
      moderator.reverse_approval_of_group(group)
      return redirect(reverse('igm-office'))

    action = request.POST.get('action')
      
    # Action: Approve an interest group
    if action == 'approve-group':
      return approve_interest_group(request, moderator, *args, **kwargs)
    
    # Action: Delete an interest group which is unapproved / suggested
    elif action == 'delete-group':
      return delete_suggested_ig(request, moderator, *args, **kwargs)

    # Action: Reverse approval of an interest group
    elif action == 'reverse-approval':
      return delete_suggested_ig(request, moderator, *args, **kwargs)
  
