from django.urls import path, include
from django.views.generic.base import TemplateView
from users.decorators import fosscell_chief_permission_required
from .views import ChiefOfficeView, IGsModeratorOfficeView

urlpatterns = [
    path('chief/', ChiefOfficeView.as_view(), name='chief-office'),
    path('interest-groups-moderator/', IGsModeratorOfficeView.as_view(), name='igm-office'),
]