from django.urls import path, include
from .views import \
    IGIndexView, \
    IGPageView, \
    IGMembersPageView, \
    IGMemberActionView, \
    EditIGView, \
    SuggestIGView, \
    IGFindView

from django.contrib.auth.decorators import login_required

urlpatterns = [
    path('index/', login_required(IGIndexView.as_view()), name='interest_groups'),
    path('find/', login_required(IGFindView.as_view()), name='find-interest-groups'),
    path('suggest/', login_required(SuggestIGView.as_view())),
    path('<int:group_pk>/', login_required(IGPageView.as_view())),
    path('<int:group_pk>/edit/', login_required(EditIGView.as_view())),
    path('<int:group_pk>/members/', login_required(IGMembersPageView.as_view())),
    path('<int:group_pk>/member-action/<str:action>/', login_required(IGMemberActionView.as_view()))
]