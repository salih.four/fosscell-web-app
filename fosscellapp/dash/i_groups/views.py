from django.views import View
from django.shortcuts import render, redirect
from i_groups.models import InterestGroup
from i_groups.forms import EditIGInfoForm, EditIGBadgeForm
from dash.templatetags.interestgroup import ig_edit_url, ig_url
from users.models import FOSSCellPermission, FOSSCellMember
from users.decorators import permission_required
from django.urls import reverse
from django.http import HttpResponse

def i_group_url(pk):
  return '/dash/interest-groups/' + pk

class IGIndexView(View):
  
  def get(self, request, *args, **kwargs):
    template = 'dash/pages/interest-groups.html'
    context = {
      'title': 'Your Interest Groups',
      'user_igs_list': InterestGroup.get_igs_of_user(request.user),
      'user_requested_igs_list': InterestGroup.get_igs_of_user(request.user, is_approved=False)
    }
    return render(request, template, context)

def get_member_action_for_status(status):
  member_action = None
  if status == 'requested':
    member_action = 'cancel-request'
  elif status == 'approved':
    member_action = 'leave'
  elif status == 'stranger':
    member_action = 'request-membership'
  return member_action

def get_ig_page_context(i_group, user, members_list=False):
  context = {}
  context['i_group'] = i_group
  context['ig_member_status'] = i_group.get_member_status(user)
  admin_perm = FOSSCellPermission.get_ig_permission(i_group, InterestGroup.ADMIN_PERM)
  context['user_is_ig_admin'] = user.verify_perm(admin_perm)
  context['member_action'] = get_member_action_for_status(context['ig_member_status'])
  if members_list:
    context['members_list'] = [i for i in i_group.get_members()]
  else:
    if context['user_is_ig_admin']:
      context['requested_members'] = i_group.get_requested_members()
  return context

class IGPageView(View):
  def get(self, request, group_pk, *args, **kwargs):
    def _get(request, self, group_pk, *args, **kwargs):
      i_group = InterestGroup.objects.get(pk=group_pk)
      template = 'dash/pages/interest-group-page.html'
      context = get_ig_page_context(i_group, request.user)
      return render(request, template, context)
    return _get(request, self, group_pk, *args, **kwargs)

class IGMembersPageView(View):
  def get(self, request, group_pk, *args, **kwargs):
    def _get(request, self, group_pk, *args, **kwargs):
      i_group = InterestGroup.objects.get(pk=group_pk)
      template = 'dash/pages/interest-group-page.html'
      context = get_ig_page_context(i_group, request.user, members_list=True)
      return render(request, template, context)
    return _get(request, self, group_pk, *args, **kwargs)
    
class IGMemberActionView(View):
  def post(self, request, group_pk, action, *args, **kwargs):
    redirect_url = request.POST.get('redirect_url')
    try:
      i_group = InterestGroup.objects.get(pk=group_pk)
    except InterestGroup.DoesNotExist:
      return redirect(reverse('interest_groups'))
    if action == 'request-membership':
      i_group.add_member(request.user, is_approved=False)
      return redirect(request.POST.get('redirect_url', ig_url(group_pk)))
    elif action == 'cancel-request' or action == 'leave':
      try:
        i_group.remove_member(request.user)
      except Exception as e:
        if str(e) == 'removing-the-only-admin':
          return HttpResponse('The feature to leave group when you are the only admin has not been implemented yet!')
        else:
          raise e
      return redirect(request.POST.get('redirect_url', ig_url(group_pk)))
    elif action == 'approve-request':
      perm = FOSSCellPermission.get_ig_permission(i_group, InterestGroup.ADMIN_PERM)
      @permission_required(perm)
      def _handle_request(request, self, i_group, *args, **kwargs):
        username = request.POST.get('username')
        try:
          fosscell_member = FOSSCellMember.objects.get(username=username)
          submit = request.POST.get('submit')
          if submit == 'Accept':
            i_group.approve_request(fosscell_member)
          elif submit == 'Delete' or 'kick-out':
            i_group.remove_member(fosscell_member)
        except FOSSCellMember.DoesNotExist:
          pass
        return redirect(redirect_url or ig_url(i_group.pk))
      return _handle_request(request, self, i_group, *args, **kwargs)

class IGFindView(View):
  def get(self, request, *args, **kwargs):
    template = 'dash/pages/interest-groups.html'
    ig_list = InterestGroup.get_suggestions_for_user(request.user)
    context = {
      'title': 'Find Interest Groups',
      'ig_suggestions_for_user': ig_list
    }
    print(context)
    return render(request, template, context)

class SuggestIGView(View):
  def post(self, request, *args, **kwargs):
    description = "Describe your group here, be aware that any user can edit or delete this suggestion until it is approved by IGS Moderator"
    
    # Creating group
    i_group = InterestGroup.objects.create(topic='New Group', description=description, status='suggested')
    i_group.save()

    # Add user to group and assign admin
    i_group.add_member(request.user, is_approved=True)
    admin_perm = FOSSCellPermission.get_ig_permission(i_group, InterestGroup.ADMIN_PERM)
    request.user.add_fosscell_permission(admin_perm)
    
    # Redirect to edit feature
    return redirect(ig_edit_url(i_group.pk, 'info'))

class EditIGView(View):
  
  FORM_TITLES = {
    'info': 'Edit group info',
    'badge': 'Change badge'
  }

  SUCCESS_RDURL = '../'
  IG_INDEX_URL = '../../index'

  def get(self, request, group_pk, *args, **kwargs):
    ig_group = InterestGroup.objects.get(pk=group_pk)
    perm = FOSSCellPermission.get_ig_permission(ig_group, InterestGroup.ADMIN_PERM)

    @permission_required(perm, raise_exception=True)
    def inner(request, self, ig_group, *args, **kwargs):
      template = 'dash/pages/django_form.html'
      form = None
      enctype = None
      if not ig_group:
        return redirect(self.IG_INDEX_URL)
      requested_form = request.GET.get('form', 'info')
      if requested_form == 'info':
        form = EditIGInfoForm({'topic': ig_group.topic, 'description':ig_group.description})
      elif requested_form == 'badge':
        enctype = 'multipart/form-data'
        form = EditIGBadgeForm()
      else:
        return redirect(self.IG_INDEX_URL)
      title = EditIGView.FORM_TITLES.get(requested_form, None)
      context = { 
        'form': form,
        'action': '',
        'method': 'post',
        'enctype': enctype,
        'title': title,
        'identifier': requested_form
        }
      return render(request, template, context)
    return inner(request, self, ig_group, *args, **kwargs)

  def post(self, request, group_pk, *args, **kwargs):
    ig_group = InterestGroup.objects.get(pk=group_pk)
    perm = FOSSCellPermission.get_ig_permission(ig_group, InterestGroup.ADMIN_PERM)
    
    @permission_required(perm, raise_exception=True)
    def inner(request, self, ig_group, *args, **kwargs):
      submitted_form = request.POST.get('identifier', None)
      form = None
      if submitted_form == 'info':
        form = EditIGInfoForm(request.POST)
        if form.is_valid():
          ig_group.topic = form.cleaned_data['topic']
          ig_group.description = form.cleaned_data['description']
          ig_group.save()
          return redirect(self.SUCCESS_RDURL)
      elif submitted_form == 'badge':
        form = EditIGBadgeForm(request.POST, request.FILES)
        if form.is_valid():
          ig_group.badge = form.cleaned_data['badge']
          ig_group.save()
          return redirect(self.SUCCESS_RDURL)
      else:
        return redirect(self.IG_INDEX_URL)
      context = { 
      'form': form,
      'action': '',
      'method': 'post',
      'enctype': enctype,
      'title': self.FORM_TITLES[identifier],
      'identifier': identifier
      }
      return render(request, template, context)
    return inner(request, self, ig_group, *args, **kwargs)
        

