from .models import FOSSCellPermission
from django.core.exceptions import PermissionDenied
from i_groups.models import InterestGroup

class FOSSCellChief():
  def __init__(self, member):
    perm = FOSSCellPermission.get_fosscell_chief_perm()
    if member.verify_perm(perm):
      self.designee = member
    else:
      raise PermissionDenied

  def designate_user_as_igs_moderator(self, user):
    perm = FOSSCellPermission.get_igs_moderator_perm()
    user.permissions.add(perm)
  
  def undesignate_user_as_igs_moderator(self, user):
    perm = FOSSCellPermission.get_igs_moderator_perm()
    user.permissions.remove(perm)

  def assign_user_as_successor(self, user):
    perm = FOSSCellPermission.get_fosscell_chief_perm()
    user.permissions.add(perm)
    self.designee.permissions.remove(perm)

class IGsModerator():
  def __init__(self, member):
    perm = FOSSCellPermission.get_igs_moderator_perm()
    if member.verify_perm(perm):
      self.designee = member
    else:
      raise PermissionDenied
  
  def assign_ig_admin(self, igroup, user):
    perm = FOSSCellPermission.get_ig_admin_permission(igroup)
    user.permissions.add(perm)
    self.designee.remove(perm)

  def get_unapproved_igs_list(self):
    u_igs_list = InterestGroup.objects.filter(status='suggested')
    return [i for i in u_igs_list]

  def approve_interest_group(self, igroup):
    igroup.mark_as_approved()

  def reverse_interest_group_approval(self, igroup):
    igroup.mark_as_suggested()

  def delete_unapproved_interest_group(self, igroup):
    igroup.delete_if_unapproved()

