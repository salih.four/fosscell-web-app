from django.db import models
from django.core.validators import RegexValidator
from django.contrib.auth.models import AbstractUser
from django_resized import ResizedImageField
import os, errno
from django.utils.translation import gettext_lazy as _
from django.templatetags.static import static

# Create your models here.
NITC_RELATIONS = [
    ('student', 'Student'),
    ('faculty', 'Faculty'),
    ('alumni', 'Alumni'),
]

PHONE_REGEX = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")

class FOSSCellPermission(models.Model):
    
    # The Kind class is deprecated, use classes that follows this class instead
    class Kind:
        IG_SPECIFIC = 'ig_specific'
        IGS = 'interest_group'
        FOSSCELL_CHIEF = 'fosscell_chief'

    # Use this instead of the Kind class
    class IGsModerator:
        kind = 'interest_group'
        codename = 'moderator'
    class IGAdmin:
        kind = 'ig_specific'
        codename = 'admin'
    class FC:
        kind = 'fosscell_chief'
        codename = 'fosscell_chief'


    MODERATOR = 'moderator'

    kind = models.CharField(max_length=20, null=False)
    i_group = models.ForeignKey(to='i_groups.InterestGroup', null=True, on_delete=models.CASCADE)
    codename = models.CharField(max_length=100, null=True)

    class Meta:
        unique_together = [['kind', 'i_group', 'codename']]

    def __init__(self, *args, **kwargs):
        self.rel_instance = None
        super(FOSSCellPermission, self).__init__(*args, **kwargs)

    @staticmethod
    def get_ig_admin_permission(i_group):
        perm = FOSSCellPermission.objects.get(
            kind=FOSSCellPermission.Kind.IG_SPECIFIC, 
            codename=FOSSCellPermission.Codes.IG_ADMIN, 
            i_group=i_group
        )
        perm.load_rel_instance_if_none()
        return perm

    @staticmethod
    def get_ig_permission(i_group, codename):
        perm = FOSSCellPermission.objects.get(kind=FOSSCellPermission.Kind.IG_SPECIFIC, codename=codename, i_group=i_group)
        if not perm:
            return perm
        perm.load_rel_instance_if_none()
        return perm

    def get_ig_permission_count(i_group, codename):
        return FOSSCellPermission.objects.filter(kind=FOSSCellPermission.Kind.IG_SPECIFIC, codename=codename, i_group=i_group).count()

    @staticmethod
    def create_ig_permission(i_group, codename):
        return FOSSCellPermission.objects.create(kind=FOSSCellPermission.Kind.IG_SPECIFIC, codename=codename, i_group=i_group)

    def load_rel_instance_if_none(self):
        if not self.rel_instance:
            if self.kind == FOSSCellPermission.Kind.IG_SPECIFIC:
                self.rel_instance = self.i_group
    
    def get_rel_instance(self):
        self.load_rel_instance_if_none()
        return self.rel_instance

    def set_rel_instance(self, obj):
        if isinstance(obj, InterestGroup):
            self.kind = FOSSCellPermission.Kind.IG_SPECIFIC
            self.t_group = obj
    
    @staticmethod
    def init_fosscell_permissions():
        try:
            FOSSCellPermission.objects.get(kind=FOSSCellPermission.Kind.FOSSCELL_CHIEF)
        except FOSSCellPermission.DoesNotExist:
            perm = FOSSCellPermission.objects.create(kind=FOSSCellPermission.Kind.FOSSCELL_CHIEF)
            perm.save()

    @staticmethod
    def get_fosscell_chief_perm():
        try:    
            return FOSSCellPermission.objects.get(kind=FOSSCellPermission.Kind.FOSSCELL_CHIEF)
        except FOSSCellPermission.DoesNotExist:
            FOSSCellPermission.init_fosscell_permissions()
            return FOSSCellPermission.objects.get(kind=FOSSCellPermission.Kind.FOSSCELL_CHIEF)

    @staticmethod
    def get_users_with_permission(perm):
        return [u for u in perm.user_set.all()]

    @staticmethod
    def get_fosscell_chief():
        perm = FOSSCellPermission.get_fosscell_chief_perm()
        query_list = FOSSCellPermission.get_users_with_permission(perm)
        if len(query_list) == 0:
            return None
        else:
            return query_list[0]

    @staticmethod
    def get_igs_moderator_perm():
        try:
            return FOSSCellPermission.objects.get(
                kind=FOSSCellPermission.IGsModerator.kind,
                codename=FOSSCellPermission.IGsModerator.codename,
            )
        except FOSSCellPermission.DoesNotExist:
            FOSSCellPermission.objects.create(
                kind=FOSSCellPermission.IGsModerator.kind,
                codename=FOSSCellPermission.IGsModerator.codename,
            ).save()
            return FOSSCellPermission.objects.get(
                kind=FOSSCellPermission.IGsModerator.kind,
                codename=FOSSCellPermission.IGsModerator.codename,
            )

    @staticmethod
    def get_igms_list():
        perm = FOSSCellPermission.get_igs_moderator_perm()
        query_list = FOSSCellPermission.get_users_with_permission(perm)
        return [i for i in query_list]

    @staticmethod
    def assign_fosscell_chief(user):
        perm = FOSSCellPermission.get_fosscell_chief_perm()
        query_list = FOSSCellPermission.get_users_with_permission(perm)
        for u in query_list:
            u.permissions.remove(perm)
        user.permissions.add(perm)

class FOSSCellPermissionsMixin(models.Model):
    permissions = models.ManyToManyField(
        FOSSCellPermission,
        verbose_name=_('fosscell permissions'),
        blank=True,
        help_text=_('Specific fosscell permissions for this user.'),
        related_name="user_set",
        related_query_name="user",
    )

    class Meta:
        abstract = True

    def get_fosscell_permissions(self):
        return set(self.permissions.all())

    def add_fosscell_permission(self, perm):
        self.permissions.add(perm)

    def verify_perm(self, perm):
        if not isinstance(perm, FOSSCellPermission):
            return False
        for p in self.get_fosscell_permissions():
            p.load_rel_instance_if_none()
            perm.load_rel_instance_if_none()
            if p.kind == perm.kind and \
                    type(p.rel_instance) == type(perm.rel_instance) and \
                    p.pk == perm.pk:
                return True
        return False

    def is_fosscell_chief(self):
        perm = FOSSCellPermission.get_fosscell_chief_perm()
        return True if self.verify_perm(perm) else False
       
class FOSSCellMember(AbstractUser, FOSSCellPermissionsMixin):
    avatar = ResizedImageField(size=[150, 150], crop=['middle', 'center'], upload_to='static/users/avatars/', null=True)
    nitc_relation = models.CharField(max_length=20, choices=NITC_RELATIONS, null=True, blank=True)
    roll_no = models.CharField(max_length=9, null=True, blank=True)
    mobile = models.CharField(validators=[PHONE_REGEX], null=True, max_length=17, blank=True) # validators should be a list

    def get_avatar_url(self):
        if self.avatar and hasattr(self.avatar, 'url'):
            return '/'+self.avatar.url
        else:
            return static('users/avatars/none.png')

    def save(self, *args, **kwargs):

        # Deleting old avatar upon avatar update
        if self.pk:
            old_data = FOSSCellMember.objects.get(pk=self.pk)
            if self.avatar != old_data.avatar:
                old_image_path = old_data.get_avatar_url()
                if old_image_path:
                    try:
                        os.remove(old_image_path)
                    except OSError as e:
                        if e.errno != errno.ENOENT:
                            raise
        super(FOSSCellMember, self).save(*args, **kwargs)
        return

    def get_prettified_nitc_relation(self):
        if not self.nitc_relation:
            return None
        for i in NITC_RELATIONS:
            if i[0] == self.nitc_relation:
                return i[1]
        return None