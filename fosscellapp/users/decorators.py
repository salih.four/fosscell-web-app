from django.contrib.auth.decorators import user_passes_test
from django.core.exceptions import PermissionDenied
from .models import FOSSCellPermission

def permission_required(perm, login_url=None, raise_exception=False):
    """
    Decorator for views that checks whether a user has a particular permission
    enabled, redirecting to the log-in page if necessary.
    If the raise_exception parameter is given the PermissionDenied exception
    is raised.
    """
    def check_perms(user):
        # First check if the user has the permission (even anon users)
        if user.verify_perm(perm):
            return True
        # In case the 403 handler should be called raise the exception
        if raise_exception:
            raise PermissionDenied
        # As the last resort, show the login form
        return False
    return user_passes_test(check_perms, login_url=login_url)

def fosscell_chief_permission_required(function=None, login_url=None, raise_exception=True):
    """
    Decorator for views that checks that the user is logged in, redirecting
    to the log-in page if necessary.
    """

    def check_perms(user):
        if not user.is_authenticated:
            return False
        perm = FOSSCellPermission.get_fosscell_chief_perm()
        # First check if the user has the permission (even anon users)
        if user.verify_perm(perm):
            return True
        # In case the 403 handler should be called raise the exception
        if raise_exception:
            raise PermissionDenied
        # As the last resort, show the login form
        return False
        
    actual_decorator = user_passes_test(
        check_perms,
        login_url=login_url,
    )
    if function:
        return actual_decorator(function)
    return actual_decorator