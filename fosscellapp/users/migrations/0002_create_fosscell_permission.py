# Generated by Django 3.0.4 on 2020-04-05 15:57

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('i_groups', '0002_interest_group__add_members_field'),
        ('users', '0001_customise_auth_user'),
    ]

    operations = [
        migrations.CreateModel(
            name='FOSSCellPermission',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('kind', models.CharField(max_length=20)),
                ('codename', models.CharField(max_length=100)),
                ('i_group', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='i_groups.InterestGroup')),
            ],
            options={
                'unique_together': {('kind', 'i_group', 'codename')},
            },
        ),
    ]
