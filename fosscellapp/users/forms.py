from django import forms
from .models import NITC_RELATIONS, PHONE_REGEX

class EditPersonalDetailsForm(forms.Form):
  first_name = forms.CharField(label='First name', max_length=150, required=False)
  last_name = forms.CharField(label='Last name', max_length=150)
  mobile = forms.CharField(label='Phone No.', validators=[PHONE_REGEX], required=False, max_length=17)

class EditNITCRelationForm(forms.Form):
  nitc_relation = forms.ChoiceField(label='NITC Relation', choices=NITC_RELATIONS)
  roll_no = forms.CharField(label='Roll No. (If Student)', min_length=9, max_length=9, required=False)

class EditAvatarForm(forms.Form):
  avatar = forms.ImageField(label='Avatar image (A size of 150x150 px expected)')